"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
var ApiService;
(function (ApiService) {
    function getSecuritySentences(requirement) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.post('http://51.178.12.108:8000/text', requirement, {
                headers: { 'Content-type': 'text/plain;' }
            });
            return response.data;
        });
    }
    ApiService.getSecuritySentences = getSecuritySentences;
    function getRecommendedStigs(requirement) {
        return __awaiter(this, void 0, void 0, function* () {
            // TODO: Make actual API call when API will be ready
            // fake api response
            return ['https://www.stigviewer.com/stig/canonical_ubuntu_16.04_lts/2020-12-09/finding/V-214961'];
        });
    }
    ApiService.getRecommendedStigs = getRecommendedStigs;
})(ApiService || (ApiService = {}));
exports.default = ApiService;
//# sourceMappingURL=apiService.js.map